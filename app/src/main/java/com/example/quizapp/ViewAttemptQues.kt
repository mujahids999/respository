package com.example.quizapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.quizapp.adapters.RVAdapterAQ
import com.example.quizapp.databinding.ActivityViewAttemptQuesBinding
import com.example.quizapp.db.PlayTask
import com.example.quizapp.db.TaskDatabase

class ViewAttemptQues : AppCompatActivity() {

    lateinit var binding: ActivityViewAttemptQuesBinding

    private var tList: List<PlayTask> = ArrayList()
            override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityViewAttemptQuesBinding.inflate(layoutInflater)
        setContentView(binding.root)

                val th = Thread(Runnable {
                    tList = TaskDatabase.getDatabase(this).playDao().getAllQues()

                })
                th.start()
                th.join()





                binding.backtodashboard.setOnClickListener {
                    onBackPressed()
                }

    }



    fun loadData()
    {
        binding.recycleListAQ.layoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.VERTICAL, false
        )

        if(tList== null)
        {
            binding.notfound.visibility= View.VISIBLE
            binding.notfound.setImageResource(R.drawable.not_found)
        }

        binding.recycleListAQ.adapter = RVAdapterAQ(tList)
    }
}