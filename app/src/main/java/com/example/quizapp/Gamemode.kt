package com.example.quizapp

import android.app.Dialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.example.quizapp.databinding.ActivityGamemodeBinding
import com.example.quizapp.sharedpreferences.PreferencesUtils
import org.json.JSONObject


class Gamemode : AppCompatActivity() {




    lateinit var binding: ActivityGamemodeBinding
    lateinit var quesList: ArrayList<QUESTION>
    var spincategory:Int = 0
    var selectedCategory: Int? = null
    var selectedDifficulty:String? = null
    var selectedType:String? = null
    var pass:String? = null
    var dialog: Dialog?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGamemodeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        quesList = ArrayList()




        var spinAdapter = ArrayAdapter.createFromResource(
            this,
            R.array.catego,
            android.R.layout.simple_spinner_item
        )

        spinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        binding.spinner.adapter = spinAdapter

        binding.spinner.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, positionc: Int, id: Long)  {
//                val spin_id: String = binding.spinner.getSelectedItem().toString()


                if(positionc == 0)
                {
                    selectedCategory= positionc
                    spincategory=0
                }
                else
                {
                    selectedCategory=positionc+8
                    spincategory=positionc
                }
                Log.e("value =", selectedCategory.toString())

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        })




        var spinAdapter2 = ArrayAdapter.createFromResource(
            this,
            R.array.difficulty,
            android.R.layout.simple_spinner_item
        )

        spinAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        binding.spinner2.adapter = spinAdapter2

        binding.spinner2.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, positiond: Int, id: Long)  {

                if(positiond == 0)
                {
                    selectedDifficulty= ""
                }
                else if(positiond == 1 )
                {
                    selectedDifficulty = "easy"
                }
                else if(positiond == 2 )
                {
                    selectedDifficulty = "medium"
                }
                else if(positiond == 3 )
                {
                    selectedDifficulty = "hard"
                }

                Log.e("value =", selectedDifficulty.toString())

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        })



        var spinAdapter3 = ArrayAdapter.createFromResource(
            this,
            R.array.type,
            android.R.layout.simple_spinner_item

        )

        spinAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        binding.spinner3.adapter = spinAdapter3

        binding.spinner3.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, positiont: Int, id: Long)  {
//                val spin_id: String = binding.spinner.getSelectedItem().toString()


                if(positiont == 0)
                {
                    selectedType= ""
                }
                else if(positiont == 1 )
                {
                    selectedType = "multiple"
                }
                else if(positiont == 2 )
                {
                    selectedType = "boolean"
                }

                Log.e("value =", selectedType.toString())

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        })


        binding.start.setOnClickListener {

            val thread = Thread(Runnable {
                networkLibrary(selectedCategory, selectedDifficulty, selectedType)
            })

            thread.start()
            thread.join()

            binding.loading.visibility = View.VISIBLE

//            showCustomDialog()
            addAttempt()

        }
        binding.backtodashboard.setOnClickListener {

            onBackPressed()
        }


    }

    fun networkLibrary( cat: Int?, diff: String?, type:String?) {


        var url: String ="https://opentdb.com/api.php?amount=10"


        if(cat!= null)
        {

                    url += "&category=$cat"

        }

        if(diff != null)
        {
            url += "&difficulty=$diff"
        }


        if(type != null)
        {
            url += "&type=$type"
        }

        AndroidNetworking.get(url)
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {

                    binding.loading.visibility = View.GONE
//                    hideCustomDialog()
                    val intent = Intent(this@Gamemode, Attempt::class.java)
                    intent.putExtra("Data" ,response.toString())
                    intent.putExtra("Category",spincategory)
                    startActivity(intent)
                    finish()
                }

                override fun onError(anError: ANError?) {

//                    hideCustomDialog()
                    Toast.makeText(this@Gamemode,"Error: ${anError?.message}", Toast.LENGTH_SHORT).show()
                    Log.e("TAG", "onError: $anError" )
                }

            })

          }



//you can use below function if you want to show loading animation in custom dialog, i have also created its xml

// private fun showCustomDialog() {
//     dialog = Dialog(this@Gamemode)
//     dialog?.let{ dialog ->
//
//         dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//         dialog.setCancelable(false)
//         dialog.setContentView(R.layout.custom_dialog_layout)
//         dialog.window?.setBackgroundDrawable(getDrawable(R.drawable.rounded_background_transparent_black))
//         dialog.show()
//
//     }
// }

//    fun hideCustomDialog(){
//
//        dialog?.let{
//            it.dismiss()
//        }
//
//    }

    fun addAttempt(){

        PreferencesUtils(this).putInt("points",(PreferencesUtils(this).getInt("points",0)+1))


    }

    override fun onBackPressed() {

        if(!binding.loading.isAnimating)
        {
            super.onBackPressed()
        }

    }


}