package com.example.quizapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.quizapp.adapters.RVAdapter
import com.example.quizapp.databinding.ActivityViewpointsBinding
import com.example.quizapp.db.PlayTask

class Viewpoints : AppCompatActivity() {
    lateinit var binding: ActivityViewpointsBinding
    private var tList: List<PlayTask> = ArrayList()
    lateinit var catname: ArrayList<String>
        var playTask = PlayTask()
    var pt: Int =0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityViewpointsBinding.inflate(layoutInflater)
        setContentView(binding.root)




        loadData()

        binding.backtodashboard.setOnClickListener {
            onBackPressed()
        }




    }


    fun loadData()
    {
        binding.recycleList.layoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.VERTICAL, false
        )

        catname = ArrayList()


        catname.add("Any Category")
        catname.add("General Knowledge")
        catname.add("Entertainment: Books")
        catname.add("Entertainment: Film")
        catname.add("Entertainment: Music")
        catname.add("Entertainment: Musicals & Theatres")
        catname.add("Entertainment: Television")
        catname.add("Entertainment: Video Games")
        catname.add("Entertainment: Board Games")
        catname.add("Science & Nature")
        catname.add("Science: Computers")
        catname.add("Science: Mathematics")
        catname.add("Mythology")
        catname.add("Sports")
        catname.add("Geography")
        catname.add("History")
        catname.add("Politics")
        catname.add("Art")
        catname.add("Celebrities")
        catname.add("Animals")
        catname.add("Vehicles")
        catname.add("Entertainment: Comics")
        catname.add("Science: Gadgets")
        catname.add("Entertainment: Japanese Anime & Manga")
        catname.add("Entertainment: Cartoon & Animations")


        binding.recycleList.adapter = RVAdapter( catname, pt )
    }
}