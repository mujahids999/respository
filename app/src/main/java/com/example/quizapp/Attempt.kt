package com.example.quizapp

import android.content.DialogInterface
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AlertDialog
import androidx.core.text.HtmlCompat
import com.example.quizapp.databinding.ActivityAttemptBinding
import com.example.quizapp.db.PlayTask
import com.example.quizapp.db.TaskDatabase
import com.google.gson.Gson

class Attempt : AppCompatActivity() {

    lateinit var binding: ActivityAttemptBinding
    lateinit var database: TaskDatabase
    lateinit var list: List<QUESTION>
    var listQuestions: ArrayList<String> = ArrayList()
    var pointsCalculation: Int =0
    private var counter: Int = 0
    private var totalpt: Int =0
    private var cat:Int = 0

    var Lines:List<String>?=null






    private var attempt: Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAttemptBinding.inflate(layoutInflater)
        setContentView(binding.root)


        Lines = resources.getStringArray(R.array.catego).asList()


        database = TaskDatabase.getDatabase(this)

        cat = intent.getIntExtra("Category",0)

        val json = intent.getStringExtra("Data")
        val gson = Gson()
        val obj3 = gson.fromJson(json, MyData::class.java)

        list = obj3.results




        Log.e("get = ", list.toString())


        Handler(Looper.getMainLooper()).postDelayed({

            binding.countdown.visibility = View.GONE

            binding.countdown.pauseAnimation()


            binding.wholeset.visibility = View.VISIBLE
            setQues()

        }, 2500) // 300


        binding.countdown.visibility = View.VISIBLE
        binding.countdown.playAnimation()


        val th4 = Thread(Runnable {
            totalpt = TaskDatabase.getDatabase(this).playDao().getNoOfPoint()

        })
        th4.start()
        th4.join()




        binding.opt1.setOnClickListener {

            disableAllOptions(false)
            checkAns(binding.opt1.text.toString())


        }


        binding.opt2.setOnClickListener {
            disableAllOptions(false)
            checkAns(binding.opt2.text.toString())


        }

        binding.opt3.setOnClickListener {
            disableAllOptions(false)
            checkAns(binding.opt3.text.toString())


        }

        binding.opt4.setOnClickListener {
            disableAllOptions(false)
            checkAns(binding.opt4.text.toString())

        }

        binding.tvshowpt.setOnClickListener {

            val intent = Intent(this, Profile::class.java)
            startActivity(intent)

        }

        binding.leavegame.setOnClickListener {

            leaveGameDialog()

        }

        binding.tvshowpt.text = "${totalpt} Points "



    }


    fun setQues() {
        if (counter < list.size) {


            binding.qno.text = "${(counter + 1)}/10"



            binding.questionCheck.text = htmlConvert(list[counter].question)



            Log.e("check ${counter}=",list[counter].correct_answer )
            listQuestions.clear()



            if (list[counter].type.equals("multiple", true)) {
                listQuestions.add(list[counter].correct_answer)
                listQuestions.add(list[counter].incorrect_answers[0])
                listQuestions.add(list[counter].incorrect_answers[1])
                listQuestions.add(list[counter].incorrect_answers[2])

                listQuestions.shuffle()

                binding.opt3.visibility = View.VISIBLE
                binding.opt4.visibility = View.VISIBLE

                binding.opt1.text = htmlConvert(listQuestions.get(0))
                binding.opt2.text = htmlConvert(listQuestions.get(1))
                binding.opt3.text = htmlConvert(listQuestions.get(2))
                binding.opt4.text = htmlConvert(listQuestions.get(3))





            } else {

                binding.opt3.visibility = View.GONE
                binding.opt4.visibility = View.GONE

                binding.opt1.text = "True"
                binding.opt2.text = "False"


            }

            disableAllOptions(true)

        } else {
            showAlertDialog()
        }

    }


    fun checkAns(option: String) {
        var playTask = PlayTask()

        val anim = AnimationUtils.loadAnimation(this, R.anim.bounceb)

        anim.setAnimationListener(object : Animation.AnimationListener{
            override fun onAnimationStart(animation: Animation?) {

            }

            override fun onAnimationEnd(animation: Animation?) {

                    counter++
                    setQues()
                    binding.ivTickCross.visibility = View.GONE


            }

            override fun onAnimationRepeat(animation: Animation?) {

            }
        })
        var currentPoint: Int =0
        if (list[counter].correct_answer.equals(option)) {


            when(list[counter].difficulty){
                "easy" -> {currentPoint=1}
                "medium" -> {currentPoint=2}
                "hard" -> {currentPoint=3}
            }

            pointsCalculation+= currentPoint


            binding.ivTickCross.setImageResource(R.drawable.ic_baseline_check_true_24)


            playTask.point = currentPoint
            playTask.questionState = true


        } else {

            binding.ivTickCross.setImageResource(R.drawable.ic_baseline_check_false)
            playTask.point = currentPoint
            playTask.questionState = false


        }

        playTask.question = list[counter].question
        playTask.correctAns = list[counter].correct_answer
        playTask.attemptedAns = option
        playTask.difficulty = list[counter].difficulty
        playTask.questionCategory = Lines?.get(cat).toString()
        addData(playTask)
        binding.ivTickCross.visibility = View.VISIBLE

        binding.ivTickCross.startAnimation(anim)





    }

    fun showAlertDialog() {

        var alertDialog = AlertDialog.Builder(this@Attempt)


        alertDialog.setTitle("Finish")
            .setMessage("You Earned ${pointsCalculation} Points in this Game")
            .setCancelable(false)
            .setPositiveButton("ok", DialogInterface.OnClickListener { dialogInterface, which ->

                dialogInterface.dismiss()

                val intent = Intent(this, Dashboard::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(intent)
                finish()
            })

        alertDialog.create().show()

    }

    fun leaveGameDialog() {

        var alertDialog = AlertDialog.Builder(this@Attempt)


        alertDialog
            .setMessage("Are you sure you want to leave ?")
            .setCancelable(false)
            .setNegativeButton("No", DialogInterface.OnClickListener { dialogInterface, which ->

                dialogInterface.cancel()


            })

            .setPositiveButton("Yes", DialogInterface.OnClickListener { dialogInterface, which ->

                dialogInterface.dismiss()

                onBackPressed()
            })

        alertDialog.create().show()

    }


    fun disableAllOptions(clickable: Boolean) {


        if (clickable) {
            binding.opt1.isEnabled = true
            binding.opt2.isEnabled = true
            binding.opt3.isEnabled = true
            binding.opt4.isEnabled = true
            binding.opt1.alpha = 1.0f
            binding.opt2.alpha = 1.0f
            binding.opt3.alpha = 1.0f
            binding.opt4.alpha = 1.0f
        } else {
            binding.opt1.isEnabled = false
            binding.opt2.isEnabled = false
            binding.opt3.isEnabled = false
            binding.opt4.isEnabled = false
            binding.opt1.alpha = 0.5f
            binding.opt2.alpha = 0.5f
            binding.opt3.alpha = 0.5f
            binding.opt4.alpha = 0.5f

        }


    }


    fun htmlConvert(list: String): CharSequence? {

        if (Build.VERSION.SDK_INT >= 24)
        {
            return HtmlCompat.fromHtml(list, HtmlCompat.FROM_HTML_MODE_LEGACY);



        }
        else
        {
            return HtmlCompat.fromHtml(list, HtmlCompat.FROM_HTML_MODE_LEGACY);

        }


    }


    fun addData(playTask: PlayTask) {

        val th = Thread(Runnable {
            val isInserted = database.playDao().insertQ(playTask)

            Log.e("Data inserted", isInserted.toString())

        })
        th.start()
        th.join()
    }

    override fun onBackPressed() {

        if(!binding.countdown.isAnimating)
        {
            super.onBackPressed()
        }

    }
}
