package com.example.quizapp.sharedpreferences

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson


class CustomSharedpreference(val  context: Context) {


    private val File_Name = "DATA"


    val sharedpreference: SharedPreferences = context.getSharedPreferences(File_Name, Context.MODE_PRIVATE)


    fun saveName(KEY: String?, name:String) {

        val dsave = sharedpreference.edit()


        dsave.putString(KEY, name)
        dsave.apply()


    }


    fun getName(KEY:String?): String?
    {

        return sharedpreference.getString(KEY,"You")






    }

}