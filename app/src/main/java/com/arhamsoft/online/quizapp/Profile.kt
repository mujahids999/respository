package com.arhamsoft.online.quizapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.arhamsoft.online.quizapp.databinding.ActivityProfileBinding
import com.arhamsoft.online.quizapp.db.PlayTask
import com.arhamsoft.online.quizapp.db.TaskDatabase
import com.arhamsoft.online.quizapp.sharedpreferences.CustomSharedpreference
import com.arhamsoft.online.quizapp.sharedpreferences.PreferencesUtils

class Profile : AppCompatActivity() {

    lateinit var binding: ActivityProfileBinding
    lateinit var sharedPreferences: CustomSharedpreference
    private var totalca: Int =0
    private var totalwa: Int =0
    private var totalpt: Int =0
    private var tList: List<PlayTask> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        sharedPreferences = CustomSharedpreference(this)



        binding.backtodashboard.setOnClickListener {

            onBackPressed()

        }

        val th5 = Thread(Runnable {
            totalpt = TaskDatabase.getDatabase(this).playDao().getNoOfPoint()

        })
        th5.start()
        th5.join()

        binding.tvshowpoints.text = "${totalpt.toString()} Points"

        binding.tvattempt.text = "Attempts (${PreferencesUtils(this).getInt("points",0)})"


        binding.ivedit.setOnClickListener {

            val intent = Intent(this, EditProfile::class.java)
            startActivity(intent)

        }

        val th = Thread(Runnable {
            totalca = TaskDatabase.getDatabase(this).playDao().getCorrectAns()

        })
        th.start()
        th.join()

        val th2 = Thread(Runnable {
            totalwa = TaskDatabase.getDatabase(this).playDao().getWrongAns()

        })
        th2.start()
        th2.join()

        val th3 = Thread(Runnable {
            tList = TaskDatabase.getDatabase(this).playDao().getAllQues()

        })
        th3.start()
        th3.join()

        val th4 = Thread(Runnable {
            totalpt = TaskDatabase.getDatabase(this).playDao().getNoOfPoint()

        })
        th4.start()
        th4.join()


        binding.tvcorrect.text = totalca.toString()
        binding.tvincorrect.text = totalwa.toString()
        binding.tvtotalques.text = tList.size.toString()
        binding.tvtotalpt.text = totalpt.toString()


    }

    override fun onResume() {
        super.onResume()
        retreiveData()
    }


    fun retreiveData(){

         binding.tvshowname.text = sharedPreferences.getName("user_name")

    }


}