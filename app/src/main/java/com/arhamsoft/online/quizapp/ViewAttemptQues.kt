package com.arhamsoft.online.quizapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.arhamsoft.online.quizapp.adapters.RVAdapterAQ
import com.arhamsoft.online.quizapp.databinding.ActivityViewAttemptQuesBinding
import com.arhamsoft.online.quizapp.db.PlayTask
import com.arhamsoft.online.quizapp.db.TaskDatabase


class ViewAttemptQues : AppCompatActivity() {

    lateinit var binding: ActivityViewAttemptQuesBinding

    private var tList: List<PlayTask> = ArrayList()
            override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityViewAttemptQuesBinding.inflate(layoutInflater)
        setContentView(binding.root)

                val th = Thread(Runnable {
                    tList = TaskDatabase.getDatabase(this).playDao().getAllQues()

                })
                th.start()
                th.join()

                loadData()


                binding.backtodashboard.setOnClickListener {
                    onBackPressed()
                }

    }



    fun loadData()
    {
        binding.recycleListAQ.layoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.VERTICAL, false
        )

        binding.recycleListAQ.adapter = RVAdapterAQ(tList)
    }
}