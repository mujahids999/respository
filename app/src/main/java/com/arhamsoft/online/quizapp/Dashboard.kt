package com.arhamsoft.online.quizapp

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.arhamsoft.online.quizapp.databinding.ActivityDashboardBinding
import com.arhamsoft.online.quizapp.db.TaskDatabase
import com.arhamsoft.online.quizapp.sharedpreferences.CustomSharedpreference
import com.arhamsoft.online.quizapp.sharedpreferences.PreferencesUtils


class Dashboard : AppCompatActivity() {

    lateinit var binding: ActivityDashboardBinding
    private var totalpt: Int =0
    lateinit var sharedPreferences: CustomSharedpreference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDashboardBinding.inflate(layoutInflater)
        setContentView(binding.root)


        sharedPreferences = CustomSharedpreference(this)


        val th4 = Thread(Runnable {
            totalpt = TaskDatabase.getDatabase(this).playDao().getNoOfPoint()

        })
        th4.start()
        th4.join()

        binding.tvshowpoints.text = "${totalpt.toString()} Points"

        binding.tvattempt.text = "Attempts (${PreferencesUtils(this).getInt("points",0)})"

        binding.start.setOnClickListener {

            val intent = Intent(this, Gamemode::class.java)
            startActivity(intent)


        }


        binding.stats.setOnClickListener {

            val intent = Intent(this, Statistics::class.java)
            startActivity(intent)

        }

        binding.setting.setOnClickListener {

            settingDialog()

        }


        binding.profile.setOnClickListener {

            val intent = Intent(this, Profile::class.java)
            startActivity(intent)

        }
        binding.viewpoints.setOnClickListener {

            val intent = Intent(this, Viewpoints::class.java)
            startActivity(intent)

        }
        binding.attemptques.setOnClickListener {

            val intent = Intent(this, ViewAttemptQues::class.java)
            startActivity(intent)

        }
    }

    override fun onResume() {
        super.onResume()
        retreiveData()
    }


    fun retreiveData(){

        binding.tvshowname.text = sharedPreferences.getName("user_name" )

    }


    fun settingDialog() {

        var alertDialog = AlertDialog.Builder(this@Dashboard)


        alertDialog
            .setMessage("Settings are not available right now.")
            .setCancelable(false)
            .setPositiveButton("ok", DialogInterface.OnClickListener { dialogInterface, which ->

                dialogInterface.dismiss()

            })

        alertDialog.create().show()

    }

}