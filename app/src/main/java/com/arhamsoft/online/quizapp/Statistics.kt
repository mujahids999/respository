package com.arhamsoft.online.quizapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.arhamsoft.online.quizapp.databinding.ActivityStatisticsBinding
import com.arhamsoft.online.quizapp.db.PlayTask
import com.arhamsoft.online.quizapp.db.TaskDatabase
import com.arhamsoft.online.quizapp.sharedpreferences.PreferencesUtils



class Statistics : AppCompatActivity() {

    lateinit var binding: ActivityStatisticsBinding
    private var totalca: Int =0
    private var totalwa: Int =0
    private var totalpt: Int =0
    private var tList: List<PlayTask> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityStatisticsBinding.inflate(layoutInflater)
        setContentView(binding.root)



        binding.backtodashboard.setOnClickListener {

            onBackPressed()
        }



        val th = Thread(Runnable {
            totalca = TaskDatabase.getDatabase(this).playDao().getCorrectAns()

        })
        th.start()
        th.join()

        val th2 = Thread(Runnable {
            totalwa = TaskDatabase.getDatabase(this).playDao().getWrongAns()

        })
        th2.start()
        th2.join()

        val th3 = Thread(Runnable {
            tList = TaskDatabase.getDatabase(this).playDao().getAllQues()

        })
        th3.start()
        th3.join()

        val th4 = Thread(Runnable {
            totalpt = TaskDatabase.getDatabase(this).playDao().getNoOfPoint()

        })
        th4.start()
        th4.join()


        binding.tvcorrect.text = totalca.toString()
        binding.tvincorrect.text = totalwa.toString()
        binding.tvtotalques.text = tList.size.toString()
        binding.tvtotalpt.text = totalpt.toString()
        binding.tvtotalatt.text = "${PreferencesUtils(this).getInt("points",0)}"




    }




}