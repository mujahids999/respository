package com.arhamsoft.online.quizapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.arhamsoft.online.quizapp.databinding.ActivityEditProfileBinding
import com.arhamsoft.online.quizapp.sharedpreferences.CustomSharedpreference

class EditProfile : AppCompatActivity() {
    lateinit var binding: ActivityEditProfileBinding

    lateinit var sharedPreferences: CustomSharedpreference

    var name:String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)



        sharedPreferences = CustomSharedpreference(this)


        retreiveData()

        binding.backtoprofile.setOnClickListener {

            onBackPressed()
        }


        binding.update.setOnClickListener {


            name = binding.etsetname.text.toString()

            updateData(name!!)



        }
    }





    fun updateData(name:String) {

        sharedPreferences.saveName("user_name", name )

       onBackPressed()

        Toast.makeText(this,"Name Saved", Toast.LENGTH_SHORT).show()

    }

    fun retreiveData(){

        binding.etsetname.setText(sharedPreferences.getName("user_name"))

    }
}