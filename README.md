# README #

This README normally define steps which are necessary to get your application in running state.

### What is this repository for? ###

* Trivizia
* 0.0.1

### Application flow ###


* Splash screen
* Main screen which include user profile(name,points,attempts),attempted questions, points by category,statistics, profile and setting
	* Attempted question(shows total no. of questions, attempted ans, correct ans attempted by the user)
	* Points by category(shows point according to category)
	* Statistics(shows total no. of correct ans, incorrect ans, question, attempts and points
	* Profile(shows user profile inw which you can edit your name and view statistics)
* game mode (choose category, difficulty and type) and then play
* category: multiple categories
* difficulty: easy = 1 point, medium = 2 point and hard = 3 point
* type: multiple choice and true/false
* if user didn't select any category, difficulty and type then any sort of questions gonna be display with random difficulties and types


### Remaining work ###

* Quick play(which shows unlimited questions with the timer of 5 seconds for each question)


### Who do I talk to? ###

* Mujahid Saleem
* mujahids999@gmail.com